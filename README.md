# Devires Framework Logging

Ativa Log de Payloads Request/Response no Spring Boot Web.

Usar a annotation **LogSensitiveData** para mascarar no log os campos com dados sensíveis.

Referências:

https://programmersought.com/article/81781392078/

https://frandorado.github.io/spring/2018/11/15/log-request-response-with-body-spring.html

### Publicar no Repositório Maven Local

> ./gradlew publishToMavenLocal

### Declarar Repositório Local nos Projetos para Usar

```
repositories {
    ...
    mavenLocal()
}
```