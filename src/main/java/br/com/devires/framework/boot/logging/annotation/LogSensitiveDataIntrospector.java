package br.com.devires.framework.boot.logging.annotation;

import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.NopAnnotationIntrospector;

public class LogSensitiveDataIntrospector extends NopAnnotationIntrospector {

	@Override
    public Object findSerializer(Annotated ann) {
        LogSensitiveData annotation = ann.getAnnotation(LogSensitiveData.class);
        if (annotation != null) {
            return LogSensitiveDataSerializer.class;
        }
        return null;
    }

}