package br.com.devires.framework.boot.logging.annotation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class LogSensitiveDataSerializer extends StdSerializer<String> {
	
	private static final String LOG_MASK = "*****";

	public LogSensitiveDataSerializer() {
         super(String.class);
    }

	@Override
	public void serialize(String value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeString(LOG_MASK);
	}

}