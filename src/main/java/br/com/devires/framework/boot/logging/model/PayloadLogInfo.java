package br.com.devires.framework.boot.logging.model;

import br.com.devires.framework.boot.logging.annotation.LogSensitiveDataIntrospector;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.AnnotationIntrospectorPair;
import lombok.*;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PayloadLogInfo {

    public enum LogType {
        REQUEST_LOG, RESPONSE_LOG;
    }

    private static final String SINGLE_LINE_REGEX = "\\s*[\\r\\n]+\\s*";

    private String method;
    private String requestURI;
    private Object body;
    private LogType type;
    private static ObjectMapper mapper;

    @Singular
    private Map<String, String[]> parameters;

    static {
        mapper = new ObjectMapper();
        AnnotationIntrospector sis = mapper.getSerializationConfig().getAnnotationIntrospector();
        AnnotationIntrospector dis = mapper.getDeserializationConfig().getAnnotationIntrospector();

        AnnotationIntrospector is1 = AnnotationIntrospectorPair.pair(sis, new LogSensitiveDataIntrospector());
        AnnotationIntrospector is2 = AnnotationIntrospectorPair.pair(dis, new LogSensitiveDataIntrospector());

        mapper.setAnnotationIntrospectors(is1, is2);
    }

    @Override
    public String toString() {

        StringBuilder logBuilder = new StringBuilder();

        if (method != null && !method.isBlank()) {
            String method = String.format("[Method: %s]", this.method);
            logBuilder.append(method);
            logBuilder.append(" ");
        }

        if (requestURI != null && !requestURI.isBlank()) {
            String uri = String.format("[URI: %s]", this.requestURI);
            logBuilder.append(uri);
            logBuilder.append(" ");
        }

        if (parameters != null && !parameters.isEmpty()) {
            try {
                String parameters = mapper.writeValueAsString(this.parameters);
                logBuilder.append("Parameters:");
                logBuilder.append(" ");
                logBuilder.append(parameters);
                logBuilder.append(" ");
            } catch (JsonProcessingException ex) {
                errorMessageBuild(logBuilder, ex);
            }
        }

        if (body != null) {
            try {
                String body = mapper.writeValueAsString(this.body);
                logBuilder.append("Body");
                logBuilder.append(" ");
                logBuilder.append(body);
            } catch (JsonProcessingException ex) {
                errorMessageBuild(logBuilder, ex);
            }
        }

        return buildStringOutput(logBuilder);
    }

    private String buildStringOutput(StringBuilder logBuilder) {
        return logBuilder.toString().replaceAll(SINGLE_LINE_REGEX, "");
    }

    private void errorMessageBuild(StringBuilder logBuilder, JsonProcessingException ex) {
        StringWriter error = new StringWriter();
        ex.printStackTrace(new PrintWriter(error));
        String fail = String.format("Fail %s body to json for log: %s", type, error);
        logBuilder.append(fail);
    }

}