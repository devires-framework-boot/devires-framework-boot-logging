package br.com.devires.framework.boot.logging.service;

import br.com.devires.framework.boot.logging.model.PayloadLogInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class PayloadLoggingService {

    public void logRequest(HttpServletRequest httpServletRequest) {
        logRequest(httpServletRequest, null);
    }

    public void logRequest(HttpServletRequest httpServletRequest, Object body) {
        PayloadLogInfo logging = PayloadLogInfo.builder()
                .type(PayloadLogInfo.LogType.REQUEST_LOG)
                .method(httpServletRequest.getMethod())
                .requestURI(httpServletRequest.getRequestURI())
                .parameters(httpServletRequest.getParameterMap())
                .body(body)
                .build();
        log.info("Request: {}", logging.toString());
    }

    public void logResponse(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object body) {
        PayloadLogInfo logging = PayloadLogInfo.builder()
                .type(PayloadLogInfo.LogType.RESPONSE_LOG)
                .method(httpServletRequest.getMethod())
                .requestURI(httpServletRequest.getRequestURI())
                .parameters(httpServletRequest.getParameterMap())
                .body(body)
                .build();
        log.info("Response: {}", logging.toString());
    }

}